package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/Kutabe/vk"
	"github.com/tidwall/gjson"
)

func errorHandler(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

func download(url string, id int64, wg *sync.WaitGroup) {
	defer wg.Done()

	var netTransport = &http.Transport{
		MaxIdleConnsPerHost: 100,
	}

	var netClient = &http.Client{
		Transport: netTransport,
	}

	fmt.Printf("Downloading %d.jpg\n", id)
	output, err := os.Create(fmt.Sprintf("%d.jpg", id))
	errorHandler(err)
	defer output.Close()

	response, err := netClient.Get(url)
	for err != nil {
		fmt.Printf("netClient.Get: Trying to handle: %s\n", err)
		response, err = netClient.Get(url)
	}
	defer response.Body.Close()

	_, err = io.Copy(output, response.Body)
	for err != nil {
		fmt.Printf("io.Copy: trying to handle: %s\n", err)
		_, err = io.Copy(output, response.Body)
	}
}

func getAlbums(user *vk.AuthResponse, parameters map[string]string) []string {
	parameters["need_system"] = "1"
	defer delete(parameters, "need_system")
	response, err := vk.Request("photos.getAlbums", parameters, user)
	errorHandler(err)
	parsedResponse := gjson.ParseBytes(response).Get("response").Array()
	albumIDs := make([]string, len(parsedResponse))
	fmt.Println("\t№  ID альбома\t\t\t\t\tИмя альбома\t\tКол-во фотографий")
	for n, i := range parsedResponse {
		albumID := gjson.Get(i.String(), "aid").String()
		albumIDs[n] = albumID
		fmt.Printf("\t%d%12s\t\t%40s\t\t", n, albumID, gjson.Get(i.String(), "title"))
		fmt.Printf("%s\n", gjson.Get(i.String(), "size"))
	}
	return albumIDs
}

func downloadAllAlbumPhotos(user *vk.AuthResponse, parameters map[string]string) {
	var offset, conCount int
	var wg sync.WaitGroup
	conCountMax := 5
	downloadsPath := fmt.Sprintf("%s/%s/%s", "downloads", parameters["owner_id"], parameters["album_id"])
	if _, err := os.Stat(downloadsPath); os.IsNotExist(err) {
		os.MkdirAll(downloadsPath, 0755)
	} else {
		files, _ := ioutil.ReadDir(downloadsPath)
		offset = len(files)
	}
	os.Chdir(downloadsPath)

	for {
		// offset используется для докачки фотографий, если в альбоме появились новые фотографии
		parameters["offset"] = fmt.Sprintf("%d", offset)
		response, err := vk.Request("photos.get", parameters, user)
		errorHandler(err)
		// VK API не может сразу вернуть исходный размер фотографии
		// Поэтому будем перебирать доступные размеры от большего к меньшему, пока не найдём наибольший
		sourceSize := []string{"src_xxxbig", "src_xxbig", "src_xbig", "src_big", "src"}
		var photoURL string
		parsedResponse := gjson.ParseBytes(response).Get("response").Array()
		for _, photo := range parsedResponse {
			for _, size := range sourceSize {
				photoURL = gjson.Get(photo.String(), size).String()
				if photoURL != "" {
					break
				}
			}
			photoID := gjson.Get(photo.String(), "pid").Int()
			conCount++
			wg.Add(1)
			go download(photoURL, photoID, &wg)
			if conCount > conCountMax {
				wg.Wait()
				conCount = 0
			}
			offset++
		}
		// VK API максимум может вернуть 1000 фотографий за один запрос
		// Если количество возвращаемых фотографий меньше - в альбоме больше нет фотографий
		if len(parsedResponse) < 1000 {
			break
		}
	}
	wg.Wait()
	fmt.Printf("Downloaded %d items\n", offset)
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Ввод учетных данных для авторизации")
	fmt.Print("Введите e-mail: ")
	login, err := reader.ReadString('\n')
	login = strings.TrimSuffix(login, "\n")
	fmt.Print("Введите пароль: ")
	password, _ := reader.ReadString('\n')
	password = strings.TrimSuffix(password, "\n")
	user, err := vk.Auth(login, password)
	if user.Error != "" {
		fmt.Println("Auth error: ", user.ErrorDescription)
		os.Exit(1)
	}
	fmt.Println("Вход в учетную запись выполнен")
	parameters := make(map[string]string)
	fmt.Print("Введите короткое имя или ID профиля/группы: ")
	id, err := reader.ReadString('\n')
	errorHandler(err)
	// Определяем, ввёл пользователь ID или короткое имя
	// Если короткое имя - пытаемся получить ID с помощью запроса utils.resolveScreenName
	// Для ID группы нужно поставить "-" перед ID
	isNumbers := regexp.MustCompile("^[0-9]").MatchString
	if !isNumbers(id) {
		tmp := map[string]string{"screen_name": id}
		response, err := vk.Request("utils.resolveScreenName", tmp, user)
		errorHandler(err)
		parsedResponse := gjson.ParseBytes(response).Get("response").String()
		objectID := gjson.Get(parsedResponse, "object_id").Int()
		objectType := gjson.Get(parsedResponse, "type").String()
		if objectType == "group" {
			id = fmt.Sprintf("-%d", objectID)
		} else {
			id = fmt.Sprintf("%d", objectID)
		}
	}
	errorHandler(err)
	parameters["owner_id"] = id
	albums := getAlbums(user, parameters)
	fmt.Print("Введите номер альбома: ")
	var albumNumberInt int64
	var albumNumberStr string
	albumNumberStr, err = reader.ReadString('\n')
	errorHandler(err)
	albumNumberStr = strings.TrimSuffix(albumNumberStr, "\n")
	albumNumberInt, _ = strconv.ParseInt(albumNumberStr, 10, 64)
	for !isNumbers(albumNumberStr) || albumNumberInt >= int64(len(albums)) {
		fmt.Println("Введено не верное значение")
		albumNumberStr, err := reader.ReadString('\n')
		errorHandler(err)
		albumNumberStr = strings.TrimSuffix(albumNumberStr, "\n")
		albumNumberInt, _ = strconv.ParseInt(albumNumberStr, 10, 64)
	}
	parameters["album_id"] = albums[albumNumberInt]
	parameters["count"] = "1000"
	parameters["rev"] = "0"
	downloadAllAlbumPhotos(user, parameters)
}
